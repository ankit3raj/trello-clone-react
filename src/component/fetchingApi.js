import axios from "axios";

const apiKey = "0415db24cffd93d71b662d4867b50830";
const apiToken =
  "dc187e4656d3d6019016e32ecf362c068d5031ff08f8849a36e81a359994f23a";

// axios default

axios.defaults.baseURL = "https://api.trello.com/1";
axios.defaults.params = {
  key: "0415db24cffd93d71b662d4867b50830",
  token: "dc187e4656d3d6019016e32ecf362c068d5031ff08f8849a36e81a359994f23a",
};
// Boards API
// to display all boards
export const displayAllBoards = async () => {
  const res = await axios.get(`/members/me/boards?,url&filter=open`);
  return res.data;
};

// to display one board
export const displayBoards = async (id) => {
  const res = await axios.get(
    `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};
// To create new Board
export const createNewBoard = async (name) => {
  console.log("in APi");
  const res = await axios.post(
    `https://api.trello.com/1/boards/?name=${name}&url`
  );
  return res.data;
};

// List API

// To Display all lists
export const displayAllLists = async (id) => {
  const res = await axios.get(
    `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// To Display the List

export const displayLists = async (id) => {
  const res = await axios.get(
    `https://api.trello.com/1/lists/${id}?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// To Create the List

export const createLists = async (name, id) => {
  const res = await axios.post(
    `https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

//To delete a List
export const DeleteLists = async (id, value) => {
  const res = await axios.put(
    `https://api.trello.com/1/lists/${id}/closed?value=${value}&key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

//Card

// To disply all cards
export const DisplayCard = async (id) => {
  const res = await axios.put(
    `https://api.trello.com/1/${id}/cards?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// To display all card
export const displayCardByListID = async (id) => {
  const res = await axios.get(
    `https://api.trello.com/1/lists/${id}/cards?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// To create a card
export const createCardByListID = async (id, name) => {
  const res = await axios.post(
    `https://api.trello.com/1/cards?idList=${id}&name=${name}&key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// To delete a card

export const deleteCard = async (cardId) => {
  const res = await axios.delete(
    `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// CheckLists

// Display all checklists api

export const displayCheckListByCardId = async (cardId) => {
  const res = await axios.get(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// Create your CheckList

export const createCheckList = async (cardId, name) => {
  const res = await axios.post(
    `https://api.trello.com/1/cards/${cardId}/checklists?name=${name}&key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// Delete Your CheckList

export const deleteCheckList = async (checkListId) => {
  const res = await axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// CheckItems Api

// Display all checkItems

export const displayCheckItems = async (checkListId) => {
  const res = await axios.get(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};

// create a new checkItems

//api.trello.com/1/checklists/{id}/checkItems?name={name}&key=APIKey&token=APIToken

export const createCheckItems = async (checkListId, name) => {
  const res = await axios.post(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${name}&key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};
// delete a newCheckItem

export const deleteCheckItems = async (checkListId, checkItemId) => {
  const res = await axios.delete(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
  );
  return res.data;
};
