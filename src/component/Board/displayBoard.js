import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import { Spinner } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { Box, Text, Input, Button, Flex } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";

class DisplayBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Board: [],
      isLoading: true,
      hasError: false,

      isOpen: false,
      BoardName: "",
    };
  }
  componentDidMount() {
    TrelloApi.displayAllBoards()
      .then((res) => {
        this.setState({
          Board: res,
          isLoading: false,
          hasError: false,
        });
      })

      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  }
  handleClick = () => {
    this.setState({ isOpen: true });
  };
  handleCloseClick = () => {
    this.setState({ isOpen: false });
  };
  handlesubmitClick = () => {
    // console.log(this.state.BoardName);
    TrelloApi.createNewBoard(this.state.BoardName)
      .then((res) => {
        const newBoard = this.state.Board.concat(res);
        this.setState({
          Board: newBoard,
          isLoading: false,
          hasError: false,

          isOpen: false,
        });
      })
      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Spinner />
        </Flex>
      );
    }
    if (this.state.hasError) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Text color="#36d7b7">Internal Server Error</Text>
        </Flex>
      );
    }
    return (
      <>
        <Flex m={4}>
          <Button onClick={this.handleClick}>Create Board</Button>

          <Modal isOpen={this.state.isOpen}>
            <ModalOverlay />

            <ModalContent>
              <ModalHeader>Create Board</ModalHeader>

              <ModalBody>
                <Input
                  placeholder=" Your Board Name"
                  onChange={(e) => {
                    this.setState({ BoardName: e.target.value });
                  }}
                />
              </ModalBody>

              <ModalFooter>
                <Button
                  colorScheme="red"
                  mr={3}
                  onClick={this.handleCloseClick}
                >
                  Close
                </Button>
                <Button colorScheme="blue" onClick={this.handlesubmitClick}>
                  Create a Board
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Flex>

        {this.state.Board.length === 0 && (
          <Text>Click on create Board to start</Text>
        )}
        <Flex border="2px solid red" wrap="wrap">
          {this.state.Board.map((item) => {
            return (
              <Link to={"/boards/" + item.id} key={item.id}>
                <Box
                  w="300px"
                  h="200px"
                  rounded="20px"
                  overflow="hidden"
                  bg="SlateBlue"
                  m={20}
                >
                  <Text align="center" color="white" mt="30%" fontWeight="900">
                    {item.name}
                  </Text>
                </Box>
              </Link>
            );
          })}
        </Flex>
      </>
    );
  }
}

export default DisplayBoard;
