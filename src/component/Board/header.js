import React, { Component } from "react";

import { GridIcon } from "chakra-ui-ionicons";
import { Heading, Button, Flex, Spacer } from "@chakra-ui/react";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}

  render() {
    return (
      <>
        {/* 172b4d */}
        <Flex bg="SlateBlue" h={20} w="100vw" borderBottom="1px solid grey">
          <GridIcon m={2} w={10} h={10} color="white" />

          <Heading as="h1" fontWeight="bold" ml={4} mt={2} color="#FAFBFC">
            Trello
          </Heading>
          <Spacer />
          <Button mr={10} mt={4}>
            Mode {this.state.ColorMode === "light" ? "Dark" : "Light"}
          </Button>
        </Flex>
      </>
    );
  }
}

export default Header;
