import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import { TrashIcon } from "chakra-ui-ionicons";
import { AddIcon } from "chakra-ui-ionicons";
import { Text, Input, Button, Flex } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";
import CheckList from "./checkList";

class CardDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: [],

      isOpen: false,
      cardName: "",
      isOpenModal: "",
      cardId: "",
      currentCardId: "",
      currentCardName: "",
    };
  }
  componentDidMount() {
    const id = this.props.id;
    TrelloApi.displayCardByListID(id)
      .then((res) => {
        this.setState({
          card: res,
        });
      })
      .catch((err) => {
        this.setState({
          err,
        });
      });
  }
  handleClick = () => {
    this.setState({ isOpen: true });
  };
  handleClickModal = (id, name) => {
    this.setState({
      isOpenModal: true,
      currentCardName: name,
      currentCardId: id,
    });
  };
  handleCloseClick = () => {
    this.setState({ isOpen: false });
  };
  handleCloseClickModal = () => {
    this.setState({
      isOpenModal: false,
    });
  };
  handlesubmitClick = () => {
    TrelloApi.createCardByListID(this.props.id, this.state.cardName)
      .then((res) => {
        const newCard = this.state.card.concat(res);

        this.setState({
          card: newCard,

          isOpen: false,
        });
      })
      .catch((err) => {
        this.setState({
          err,
        });
      });
  };

  handleDelete = (id) => {
    TrelloApi.deleteCard(id)
      .then(() => {
        const newData = this.state.card.filter((res) => res.id !== id);
        this.setState({
          card: newData,

          isOpen: false,
        });
      })

      .catch((err) => {
        this.setState({
          err,
        });
      });
  };

  render() {
    return (
      <>
        <Flex direction="column" w="250px" justify="center" m={2}>
          {this.state.card.map((item) => {
            return (
              <Flex
                key={item.id}
                justify="space-between"
                align="center"
                m={2}
                bg="white"
              >
                <Text
                  border="1px solid white"
                  m={2}
                  fontWeight="bold"
                  w="100%"
                  h={12}
                  bg="white"
                  onClick={() => {
                    this.handleClickModal(item.id, item.name);
                  }}
                >
                  {item.name}
                </Text>
                <Modal isOpen={this.state.isOpenModal} size="xl" bg="#D8D9CF">
                  <ModalOverlay />
                  <CheckList
                    id={this.state.currentCardId}
                    name={this.state.currentCardName}
                    isOpenModal={this.state.isOpenModal}
                    handleClose={this.handleCloseClickModal}
                  />
                </Modal>
                <Button
                  bg="white"
                  onClick={() => {
                    this.handleDelete(item.id, item.idList);
                  }}
                >
                  <TrashIcon w={7} h={8} />
                </Button>
              </Flex>
            );
          })}
          <Button
            mt={2.5}
            ml={2}
            min-width="280px"
            onClick={this.handleClick}
            fontSize="1.4rem"
            display="flex"
            justify="start"
            border="1px solid black"
          >
            <AddIcon w={8} h={8} />
            Create a Card
          </Button>

          <Modal isOpen={this.state.isOpen}>
            <ModalOverlay />

            <ModalContent>
              <ModalHeader>Create a Card</ModalHeader>

              <ModalBody>
                <Input
                  placeholder=" Your Card Name"
                  onChange={(e) => {
                    this.setState({ cardName: e.target.value });
                  }}
                />
              </ModalBody>

              <ModalFooter>
                <Button
                  colorScheme="red"
                  mr={3}
                  onClick={this.handleCloseClick}
                >
                  Close
                </Button>
                <Button colorScheme="blue" onClick={this.handlesubmitClick}>
                  Create a Card
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Flex>
      </>
    );
  }
}

export default CardDetails;
