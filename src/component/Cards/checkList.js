import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import { TrashIcon } from "chakra-ui-ionicons";
import { Text, Input, Button, Flex } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";

import { Checkbox } from "@chakra-ui/react";
import { Progress } from "@chakra-ui/react";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkList: [],
      isOpen: false,
      cardName: "",
      isOpenCheckList: false,
      checkListName: "",
      currentCheckListId: "",
      checkItemsName: "",
      checkedItems: "",
    };
  }
  componentDidMount() {
    const id = this.props.id;
    console.log(id);
    TrelloApi.displayCheckListByCardId(id)
      .then((res) => {
        this.setState({
          checkList: res,
          ColorMode: null,
        });
      })
      .catch((err) => {
        this.setState({
          err,
        });
      });
  }

  handleClickForCheckItems = (id) => {
    this.setState({ currentCheckListId: id });
  };

  handleClickForCheckList = () => {
    this.setState({ isOpenCheckList: true });
  };
  handleClickForCheckListClose = () => {
    this.setState({ isOpenCheckList: false });
  };
  handleClose = () => {
    this.setState({ isOpen: false });
  };
  handleSubmitClick = () => {
    TrelloApi.createCheckList(this.props.id, this.state.checkListName).then(
      (res) => {
        const newCheckList = this.state.checkList.concat(res);
        this.setState({ checkList: newCheckList, isOpenCheckList: false });
      }
    );
  };
  handleDelete = (id) => {
    TrelloApi.deleteCheckList(id).then((res) => {
      const newData = this.state.checkList.filter((item) => item.id !== id);
      this.setState({ checkList: newData });
    });
  };
  handleCreateItems = (id) => {
    TrelloApi.createCheckItems(id, this.state.checkItemsName).then((res) => {
      const newData = this.state.checkList.map((e) =>
        e.id === id ? { ...e, checkItems: e.checkItems.concat(res) } : e
      );
      this.setState({ checkList: newData, checkItemsName: "" });
    });
  };
  handleDeleteItems = (idCheckList, idCheckItems) => {
    TrelloApi.deleteCheckItems(idCheckList, idCheckItems).then((res) => {
      const newCheckList = this.state.checkList.map((e) =>
        e.id === idCheckList
          ? {
              ...e,
              checkItems: e.checkItems.filter(
                (inner) => inner.id !== idCheckItems
              ),
            }
          : e
      );
      this.setState({ checkList: newCheckList });
    });
  };

  handleCheckBox = (idCheckList, idCheckItems, state) => {
    const newCheckList = this.state.checkList.map((e) =>
      e.id === idCheckList
        ? {
            ...e,
            checkItems: e.checkItems.map((ci) =>
              ci.id === idCheckItems ? { ...ci, state: state } : ci
            ),
          }
        : e
    );
    this.setState({ checkList: newCheckList });
  };

  render() {
    return (
      <>
        <ModalContent>
          <ModalHeader>
            {" "}
            <Text as="h1" color="#E26868" fontSize="1.3rem">
              {this.props.name}
            </Text>
          </ModalHeader>
          <ModalBody>
            <Button m={4} onClick={this.handleClickForCheckList}>
              Create CheckList
            </Button>
            <Modal isOpen={this.state.isOpenCheckList}>
              <ModalOverlay />

              <ModalContent>
                <ModalHeader> Create Your CheckList</ModalHeader>

                <ModalBody>
                  <Input
                    placeholder=" Your checkList Name"
                    onChange={(e) => {
                      this.setState({ checkListName: e.target.value });
                    }}
                  />
                </ModalBody>

                <ModalFooter>
                  <Button
                    colorScheme="red"
                    mr={3}
                    onClick={this.handleClickForCheckListClose}
                  >
                    Close
                  </Button>
                  <Button colorScheme="blue" onClick={this.handleSubmitClick}>
                    Create your CheckList
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </ModalBody>
          {this.state.checkList.map((item) => {
            return (
              <>
                <Flex direction="column" key={item.id}>
                  <Flex
                    border="1px solid black"
                    w="auto"
                    justify="space-between"
                    bg="#FF8787"
                    m={3}
                  >
                    <Text fontSize="1.1rem" fontWeight="bold" m={3}>
                      {item.name}
                    </Text>
                    <Button
                      m={2}
                      onClick={() => {
                        this.handleDelete(item.id);
                      }}
                    >
                      <TrashIcon w={6} h={6} m={2} />
                    </Button>
                  </Flex>
                  <Flex m={4}>
                    <Input
                      placeholder="CardItems Name"
                      value={this.state.checkItemsName}
                      onChange={(e) =>
                        this.setState({ checkItemsName: e.target.value })
                      }
                    />
                    <Button
                      onClick={() => {
                        this.handleCreateItems(item.id);
                      }}
                    >
                      Add Item
                    </Button>
                  </Flex>
                </Flex>
                <Flex direction="column">
                  {item.checkItems.length ? (
                    <Flex direction="column" m={2} border="1px solid black">
                      <Text as="h1" ml={4} fontWeight="bold">
                        {(
                          (item.checkItems.filter(
                            (item) => item.state === "complete"
                          ).length /
                            item.checkItems.length) *
                          100
                        ).toFixed(2)}
                        %
                      </Text>
                      <Progress
                        hasStripe
                        m={2}
                        value={
                          (item.checkItems.filter(
                            (item) => item.state === "complete"
                          ).length /
                            item.checkItems.length) *
                          100
                        }
                      />
                    </Flex>
                  ) : (
                    "No items"
                  )}
                  {item.checkItems.map((ele) => {
                    return (
                      <Flex
                        key={ele.id}
                        justify="space-between"
                        border="1px solid blue"
                        m={4}
                        w="auto"
                      >
                        <Checkbox
                          m={2}
                          value={ele.state === "complete"}
                          onChange={(e) => {
                            this.handleCheckBox(
                              item.id,
                              ele.id,
                              e.target.checked ? "complete" : "incomplete"
                            );
                          }}
                        >
                          <Text as="h1" fontWeight="bold" fontSize="1rem">
                            {ele.name} - {ele.state}
                          </Text>
                        </Checkbox>
                        <Button
                          m={2}
                          onClick={() =>
                            this.handleDeleteItems(item.id, ele.id)
                          }
                        >
                          <TrashIcon w={6} h={6} />
                        </Button>
                      </Flex>
                    );
                  })}
                </Flex>
              </>
            );
          })}

          <ModalFooter>
            <Button colorScheme="red" mr={3} onClick={this.props.handleClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </>
    );
  }
}

export default CheckList;
