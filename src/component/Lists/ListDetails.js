import React, { Component } from "react";
import * as TrelloApi from "../fetchingApi";
import CardDetails from "../Cards/cardDetails";
import Header from "../Board/header";
import { TrashIcon } from "chakra-ui-ionicons";
import { AddIcon } from "chakra-ui-ionicons";
import { Box, Text, Input, Button, Flex } from "@chakra-ui/react";
import { Spinner } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";

class ListDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Lists: [],
      isLoading: true,
      hasError: false,

      isOpen: false,
      ListName: "",
    };
  }
  componentDidMount() {
    const { boardid } = this.props.match.params;
    TrelloApi.displayAllLists(boardid)
      .then((res) => {
        this.setState({
          Lists: res,
          isLoading: false,
          hasError: false,
        });
      })
      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  }
  handleClick = () => {
    this.setState({ isOpen: true });
  };
  handleCloseClick = () => {
    this.setState({ isOpen: false });
  };
  handlesubmitClick = () => {
    TrelloApi.createLists(this.state.ListName, this.props.match.params.boardid)
      .then((res) => {
        const newList = this.state.Lists.concat(res);
        this.setState({
          Lists: newList,
          isLoading: false,
          hasError: false,

          isOpen: false,
        });
      })
      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  };

  handleDelete = (id, boardId) => {
    TrelloApi.DeleteLists(id, true)
      .then((res) => {
        const newData = this.state.Lists.filter((res) => res.id !== id);
        this.setState({
          Lists: newData,
          isLoading: false,
          hasError: false,

          isOpen: false,
        });
      })

      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Spinner />
        </Flex>
      );
    }
    if (this.state.hasError) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Text color="#36d7b7">Internal Server Error</Text>
        </Flex>
      );
    }
    return (
      <Box bg="SlateBlue">
        <Header />
        {this.state.Lists === 0 && <Text>Click on Add List to start</Text>}
        <Flex
          h="100vh"
          position="absolute"
          overflowX="auto"
          bg="SlateBlue"
          justify="start"
          align="start"
        >
          {this.state.Lists.map((item) => {
            return (
              <Flex
                key={item.id}
                min-width="280px"
                position="relative"
                bg="#dfe1e6"
                m="10px 5px"
                ml={6}
                direction="column"
              >
                <Flex outline="none" justify="space-between">
                  <Text
                    color="Black"
                    fontSize="1.3rem"
                    ml={2}
                    mt={2}
                    fontWeight="600"
                  >
                    {item.name}
                  </Text>
                  <Button
                    m={2}
                    p={3}
                    onClick={() => {
                      this.handleDelete(item.id, item.idBoard);
                    }}
                  >
                    <TrashIcon w={6} h={6} m={2} />
                  </Button>
                </Flex>
                <Flex>
                  <CardDetails id={item.id} />
                </Flex>
              </Flex>
            );
          })}

          <Button
            mt={2.5}
            ml={2}
            min-width="280px"
            onClick={this.handleClick}
            fontSize="1.4rem"
            display="flex"
            justify="start"
            border="1px solid black"
          >
            <AddIcon w={8} h={8} />
            Add Lists
          </Button>

          <Modal isOpen={this.state.isOpen}>
            <ModalOverlay />

            <ModalContent>
              <ModalHeader>Add a Lists</ModalHeader>

              <ModalBody>
                <Input
                  border="2rem"
                  caret-color="red"
                  placeholder=" Your Lists Name"
                  onChange={(e) => {
                    this.setState({ ListName: e.target.value });
                  }}
                />
              </ModalBody>

              <ModalFooter>
                <Button
                  colorScheme="red"
                  mr={3}
                  onClick={this.handleCloseClick}
                >
                  Close
                </Button>
                <Button colorScheme="blue" onClick={this.handlesubmitClick}>
                  Add a List
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Flex>
      </Box>
    );
  }
}

export default ListDetails;
