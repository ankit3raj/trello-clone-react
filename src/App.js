import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import BoardDetails from "./component/Board/BoardDetails";
import ListDetails from "./component/Lists/ListDetails";
import { Redirect } from "react-router-dom";
import "./App.css";

class App extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Redirect exact from="/" to="/boards" />
            <Route exact path="/boards" component={BoardDetails} />

            <Route path="/boards/:boardid" component={ListDetails} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
